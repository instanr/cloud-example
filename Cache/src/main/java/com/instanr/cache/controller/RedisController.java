package com.instanr.cache.controller;

import com.instanr.cache.redis.RedisService;
import com.instanr.common.beans.RedisSaveModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    RedisService redisService;

    @PostMapping("/save")
    public boolean save(@RequestBody RedisSaveModel model) {
        if (model.getDuration() == null) {
            redisService.set(model.getKey(), model.getData());
        } else {
            redisService.set(model.getKey(), model.getData(), model.getDuration());
        }
        return true;
    }

    @PostMapping("/hasSave")
    public boolean hasSave(@RequestBody RedisSaveModel model) {
        if (model.getData() instanceof Map) {
            Map<String, Object> data = (Map<String, Object>) model.getData();
            if (model.getDuration() == null) {
                data.keySet().forEach(key -> {
                    redisService.hSet(model.getKey(), key, data.get(key));
                });

            } else {
                data.keySet().forEach(key -> {
                    redisService.hSet(model.getKey(), key, data.get(key), model.getDuration());
                });
            }
        }
        return true;
    }

    @PostMapping("/removeKey")
    public boolean removeKey(@RequestBody RedisSaveModel model) {
        return redisService.del(model.getKey());
    }

    @PostMapping("getData")
    public RedisSaveModel getData(@RequestBody RedisSaveModel query) {
        RedisSaveModel redisSaveModel = new RedisSaveModel(
                query.getKey(),
                redisService.get(query.getKey()),
                null
        );
        return redisSaveModel;
    }

    @PostMapping("getHasData")
    public RedisSaveModel getHasData(@RequestBody RedisSaveModel query) {
        RedisSaveModel redisSaveModel = new RedisSaveModel();
        if (query.getData() != null) {
            redisSaveModel = new RedisSaveModel(
                    query.getKey(),
                    redisService.hGet(query.getKey(), (String) query.getData()),
                    null
            );
        }
        return redisSaveModel;
    }
}

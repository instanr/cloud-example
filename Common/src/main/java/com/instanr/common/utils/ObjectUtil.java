package com.instanr.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public class ObjectUtil {

    public static <T> T objectToJavaObject(Object data, Class<T> clazz){
        if (! (data instanceof Map)) {
            return null;
        }
        String s = JSON.toJSONString(data);
        JSONObject jsonObject = JSON.parseObject(s);
        return JSONObject.toJavaObject(jsonObject, clazz);
    }
}

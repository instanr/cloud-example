package com.instanr.common.utils;

import com.alibaba.fastjson.JSON;
import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ResponseUtil {

    public static void out(@NotNull HttpServletResponse response, Object data) throws IOException {
        String s = JSON.toJSONString(data);
        response.getOutputStream().write(s.getBytes(StandardCharsets.UTF_8));
    }
}

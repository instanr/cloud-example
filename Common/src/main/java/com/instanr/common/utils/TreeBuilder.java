package com.instanr.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/***
 * 树状结构生成
 * @author instanr
 * @since 20211005
 */
public class TreeBuilder {

    private static Method primaryMethod;
    private static Method relevanceFieldMethod;
    private static Method setChildrenMethod;
    private static Method getChildrenMethod;

    /***
     * 将List组树
     * @param list 组树的List
     * @param primaryKey 树的主键
     * @param relevanceField 相关联的键
     * @param childrenName 子节点的名称
     * @param <T> 泛型：传入的数据类型
     * @throws NoSuchMethodException 一般是没有这个方法（没有getter）
     * @throws InvocationTargetException 目标异常
     * @throws IllegalAccessException 非法调用，参数不合法
     */
    public static <T> List<T> build(List<T> list, String primaryKey, String relevanceField, String childrenName)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        if (list == null || list.size() == 0) {
            return list;
        }

        Class<?> itemClass = list.get(0).getClass();
        formatMustFields(itemClass, primaryKey, relevanceField, childrenName);
        List<Object> rootRelevanceFieldList = getRootRelevanceFieldList(list);
        List<T> rootItemList = new ArrayList<>();
        // 循环列表取出第一层的所有节点
        for (T t : list) {
            Object itemRelevanceField = relevanceFieldMethod.invoke(t);
            boolean isRoot = false;
            for (Object o : rootRelevanceFieldList) {
                if (itemRelevanceField.equals(o)) {
                    isRoot = true;
                    break;
                }
            }
            if (isRoot) {
                rootItemList.add(t);
            }
        }
        // 开始组数
        buildTree(rootItemList, list);
        return rootItemList;
    }

    /***
     * 获取必要参数
     * @param itemClass 数组元素的Class
     * @param primaryKey 主键名称
     * @param relevanceField 关联键名称
     * @throws NoSuchMethodException 一般是没有这个方法（没有getter）
     */
    private static void formatMustFields(Class<?> itemClass, String primaryKey, String relevanceField, String childrenName)
            throws NoSuchMethodException {
        String getPrimaryMethodName = "get" + primaryKey.substring(0, 1).toUpperCase() + primaryKey.substring(1);
        String getRelevanceField = "get" + relevanceField.substring(0, 1).toUpperCase() + relevanceField.substring(1);
        String getChildrenMethodName = "get" + childrenName.substring(0, 1).toUpperCase() + childrenName.substring(1);
        String setChildrenMethodName = "set" + childrenName.substring(0, 1).toUpperCase() + childrenName.substring(1);
        primaryMethod = itemClass.getMethod(getPrimaryMethodName);
        relevanceFieldMethod = itemClass.getMethod(getRelevanceField);
        getChildrenMethod = itemClass.getMethod(getChildrenMethodName);
        setChildrenMethod = itemClass.getMethod(setChildrenMethodName, List.class);
    }

    /***
     * 获取第一层的关联键列表
     * @param list 列表
     * @param <T> 任意类型
     * @return 返回第一层的关联键列表
     */
    public static <T> List<Object> getRootRelevanceFieldList(List<T> list){
        // 取出所有的主键和关联键
        List<Object> primaryKeyList = new ArrayList<>();
        List<Object> relevanceFieldList = new ArrayList<>();
        for (T item : list) {
            try {
                primaryKeyList.add(primaryMethod.invoke(item));
                relevanceFieldList.add(relevanceFieldMethod.invoke(item));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        // 主键和关联键相比较，关联键中没有相对应的主键的就是第一层
        List<Object> rootRelevanceFields = new ArrayList<>();
        for (Object relevance : relevanceFieldList) {
            boolean hasThisKey = false;
            for (Object primary : primaryKeyList) {
                if (relevance.toString().equals(primary.toString())) {
                    hasThisKey = true;
                }
            }
            if (!hasThisKey) {
                rootRelevanceFields.add(relevance);
            }
        }
        // 关联键去重

        // 这是所有的没有上级的关联键
        List<Object> rootRelevanceFieldList = new ArrayList<>();
        for (Object o : rootRelevanceFields) {
            if (!rootRelevanceFieldList.contains(o)) {
                rootRelevanceFieldList.add(o);
            }
        }
        return rootRelevanceFieldList;
    }

    /***
     * 开始组树
     * @param rootList 上一步得到的第一层的列表
     * @param oldList 原数组
     * @param <T> 传入的List的数据类型
     */
    private static <T> void buildTree(List<T> rootList, List<T> oldList) throws InvocationTargetException, IllegalAccessException {
        List<T> surplusList = new ArrayList<>();
        surplusList.addAll(oldList);
        for (T tootItem : rootList) {
            Object primaryKey = primaryMethod.invoke(tootItem);
            List<T> childrenList = new ArrayList<>();
            for (T surplusItem : oldList) {
                Object relevanceField = relevanceFieldMethod.invoke(surplusItem);
                if (primaryKey.equals(relevanceField)) {
                    childrenList.add(surplusItem);
                    surplusList.remove(surplusItem);
                }
            }
            setChildrenMethod.invoke(tootItem, childrenList);
        }
        if (surplusList.size() > 0) {
            for (T tootItem : rootList) {
                List<T> childrenList = (List<T>) getChildrenMethod.invoke(tootItem);
                buildTree(childrenList, surplusList);
            }
        }
    }

    /*public static void main(String[] args) {
        List<Tree> treeList = new ArrayList<>();
        Tree tree1 = new Tree(1, "1", 0);
        Tree tree2 = new Tree(2, "2", 1);
        Tree tree3 = new Tree(3, "3", 1);
        Tree tree4 = new Tree(4, "4", 1);
        Tree tree5 = new Tree(5, "5", 2);
        Tree tree6 = new Tree(6, "6", 2);
        Tree tree7 = new Tree(7, "7", 6);
        Tree tree8 = new Tree(8, "8", 6);
        Tree tree9 = new Tree(9, "9", 8);
        Tree tree10 = new Tree(10, "10", 0);
        Tree tree11 = new Tree(11, "11", 0);
        treeList.add(tree1);
        treeList.add(tree2);
        treeList.add(tree3);
        treeList.add(tree4);
        treeList.add(tree5);
        treeList.add(tree6);
        treeList.add(tree7);
        treeList.add(tree8);
        treeList.add(tree9);
        treeList.add(tree10);
        treeList.add(tree11);
        try {
            List<Tree> build = build(treeList, "id", "parentId", "children");
            System.out.println(build);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }*/
}

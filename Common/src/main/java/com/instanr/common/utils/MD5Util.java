package com.instanr.common.utils;


import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/***
 * md5工具类
 * @author instanr
 * @since 2021-10-22
 */
public class MD5Util {

    /***
     * md5加密(带盐)
     * @param source 明文
     * @param salt 盐值
     * @return 密文
     */
    public static String md5Encryption(String source,String salt){
        byte[] md5Digest = DigestUtils.md5Digest(source.getBytes(StandardCharsets.UTF_8));


        String firstDigest = DigestUtils.appendMd5DigestAsHex(
                md5Digest,
                new StringBuilder(salt)
        ).toString();

        byte[] secondDigest = DigestUtils.md5Digest(firstDigest.getBytes(StandardCharsets.UTF_8));

        return DigestUtils.appendMd5DigestAsHex(
                secondDigest,
                new StringBuilder()
        ).toString();
    }
}

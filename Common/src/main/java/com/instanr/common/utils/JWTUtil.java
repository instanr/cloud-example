package com.instanr.common.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

/***
 * JWT工具类
 * @author instanr
 * @since 2021-10-25
 */
public class JWTUtil {


    /**
     * 生成签名
     * @param username 用户名
     * @param secret 用户的密码
     * @return 加密的token
     */
    public static String sign(String username, String secret, Long expireTime) {
        Date date = new Date(System.currentTimeMillis()+expireTime);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        /*try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            // 附带username信息
            return JWT.create()
                    .withClaim("username", username)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            return null;
        }*/
        try {
            return JWT.create()
                    .withClaim("username", username)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (JWTCreationException exception){
            return null;
        }
    }

    /***
     * 验证签名
     * @param username 用户名
     * @param token 传入的token
     * @param secret token的秘钥
     */
    public static Boolean verify(String username, String token, String secret) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("username", username)
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (JWTVerificationException exception){
            return false;
            //throw new ServiceException(ErrorCodeEnum.FORBIDDEN, "Token 验证不通过");
        }
    }


    /**
     * 判断过期
     * @param token
     * @return
     */
    public static boolean isExpire(String token){
        DecodedJWT jwt = JWT.decode(token);
        return System.currentTimeMillis()>jwt.getExpiresAt().getTime();
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     * <br>
     * 如果这里返回了两个空串，说明JWTToken有问题
     * @return token中包含的用户名和id 数组长度为2，[0]：username，[1]：id
     */
    public static @NotNull String[] getUsername(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString().split("-");
        } catch (JWTDecodeException e) {
            return new String[]{"", ""};
        }
    }

    public static void main(String[] args) {
        String sign = sign("admin", "aklsjdlkahfk", 666666L);
        System.out.println(sign);
        verify("admin", sign, "aklsjdlkahfk");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

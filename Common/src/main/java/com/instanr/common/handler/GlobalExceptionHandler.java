package com.instanr.common.handler;

import com.instanr.common.beans.ResponseBean;
import com.instanr.common.exception.RunningException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

/**
 * 全局异常处理类<br/>
 * 在单独模块中需自己写一个类来继承此类，并写上注解@RestControllerAdvice
 * 只有Web工程才可以使用
 * @author instanr
 * @since 2022-03-01
 * @version  1.0
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    /***
     * 处理请求参数格式错误 @RequestBody上validate失败后抛出的异常是MethodArgumentNotValidException异常。
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseBean MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining());
        return ResponseBean.error(message);
    }

    /**
     * 处理servlet异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = ServletException.class)
    @ResponseBody
    public ResponseBean servletExceptionHandler(HttpServletRequest req, ServletException e) {
        log.error("web服务器异常 {}", e.getMessage());
        return ResponseBean.error(e.getMessage());
    }

    /**
     * 处理自定义的业务异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = RunningException.class)
    @ResponseBody
    public ResponseBean bizExceptionHandler(HttpServletRequest req, RunningException e) {
        log.error("业务异常=>{}", e.getErrorMsg());
        return ResponseBean.error(e.getErrorCode(), e.getErrorMsg());
    }

    /**
     * 捕捉其他所有异常
     *
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseBean globalException(HttpServletRequest request, Throwable ex) {
        String message = ex.getMessage();
        log.error("系统异常=>{}", ex.getMessage());
        ex.printStackTrace();
        return new ResponseBean(getStatus(request).value(), "系统异常" + message, null);
    }

    /**
     * shiro的异常 因为不需要Shiro了，所以这一段被注释掉了
     * @param e
     * @return
     */
    /* @ExceptionHandler(ShiroException.class)
    public ResponseBean handle401(ShiroException e) {
        log.error("shiro异常=>{}", e.getMessage());
        return new ResponseBean(401, e.getMessage(), null);
    }*/


    /**
     * 获取状态码
     *
     * @param request
     * @return
     */
    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }
}


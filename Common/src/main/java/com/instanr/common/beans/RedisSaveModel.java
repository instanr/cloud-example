package com.instanr.common.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.Map;

/***
 * 通用Redis对象
 * 存取Redis时使用
 * @author instanr
 * @since 2022-03-24
 */
@Data
public class RedisSaveModel {
    private String key;
    private Object data;
    private Long duration;

    public RedisSaveModel(){

    }

    public RedisSaveModel(String key){
        this.key = key;
    }
    public RedisSaveModel(String key, Object data){
        this.key = key;
        this.data = data;
    }
    public RedisSaveModel(String key, Object data, Long duration) {
        this.key = key;
        this.data = data;
        this.duration = duration;
    }

    public <T> T toJavaObject(Class<T> clazz){
        if (! (this.data instanceof Map)) {
            return null;
        }
        String s = JSON.toJSONString(data);
        JSONObject jsonObject = JSON.parseObject(s);
        return JSONObject.toJavaObject(jsonObject, clazz);
    }
}

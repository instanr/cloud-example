package com.instanr.common.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.instanr.common.utils.AesUtil;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

/***
 * 通用返回包装对象
 * @author instanr
 * @since 2022-02-18
 */
@Data
public class ResponseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /***
     * 200:操作成功  500：操作失败
     */
    private int code;

    /***
     * 返回信息
     */
    private String msg;

    /***
     * 返回的数据
     */
    private Object data;

    public ResponseBean() {
    }

    public ResponseBean(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ResponseBean error(String message) {
        return new ResponseBean(500, message, null);
    }

    public static ResponseBean error(int code, String message) {
        return new ResponseBean(code, message, null);
    }

    public static ResponseBean success(Object data) {
        return new ResponseBean(200, "操作成功", data);
    }

    public static ResponseBean success() {
        return new ResponseBean(200, "操作成功", null);
    }

    public static ResponseBean success(Object data, String message) {
        return new ResponseBean(200, message, data);
    }

    public <T> T toJavaObject(Class<T> clazz){
        if (! (this.data instanceof Map)) {
            return null;
        }
        String s = JSON.toJSONString(data);
        JSONObject jsonObject = JSON.parseObject(s);
        return JSONObject.toJavaObject(jsonObject, clazz);
    }

    public String toString() {
        return JSON.toJSONString(this);
    }
}

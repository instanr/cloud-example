package com.instanr.common.beans;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@Data()
public class ActiveUser implements Serializable {


    private static final long serialVersionUID = 1L;

    /***
     * 用户ID
     */
    private Long id;
    /***
     * 用户名
     */
    private String username;
    /***
     * 昵称
     */
    private String nickname;
    /***
     * 邮箱
     */
    private String email;
    /***
     * 头像
     */
    private String avatar;
    /***
     * 联系电话
     */
    private String phoneNumber;

    /***
     * 性别 0男 1女 2保密
     */
    private Integer sex;
    /***
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birth;
    /***
     * 部门id
     */
    private Long departmentId;

    /***
     * 角色列表
     */
    List<SysRole> roleList;

    /***
     * 所拥有的菜单列表
     */
    List<SysMenu> menuList;

    /***
     * 所拥有的的按钮列表
     */
    List<String> permissions;
}

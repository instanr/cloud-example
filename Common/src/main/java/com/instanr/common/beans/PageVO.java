package com.instanr.common.beans;

import lombok.Data;

import java.util.List;

@Data
public class PageVO<T> {

    /***
     * 总数
     */
    private Long total;

    /***
     * 查出来的列表
     */
    private List<T> data;


    public PageVO(){}

    public PageVO(Long total, List<T> data) {
        this.data = data;
        this.total = total;
    }
}

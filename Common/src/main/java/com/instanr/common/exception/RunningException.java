package com.instanr.common.exception;

import com.instanr.common.eunm.ErrorCodeEnum;
import lombok.Data;

@Data
public class RunningException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    protected int errorCode;

    /**
     * 错误信息
     */
    protected String errorMsg;

    public RunningException() {
        super();
    }

    public RunningException(String message) {
        super(message);
        this.errorMsg=message;
    }



    public RunningException(ErrorCodeEnum error) {
        this.errorCode = error.getResultCode();
        this.errorMsg = error.getResultMsg();
    }

    public RunningException(ErrorCodeEnum error, String message) {
        this.errorCode = error.getResultCode();
        this.errorMsg = message;
    }
}

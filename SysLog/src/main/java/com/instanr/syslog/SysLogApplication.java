package com.instanr.syslog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class SysLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(SysLogApplication.class, args);
        System.out.printf("服务: %s 启动成功!\n", "SysLog");
    }

}

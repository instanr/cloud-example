package com.instanr.syslog.controller;

import com.instanr.common.beans.ResponseBean;
import com.instanr.syslog.service.SysLogService;
import com.instanr.syslogprovider.db.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sysLog")
@AllArgsConstructor
public class SysLogController {

    private SysLogService sysLogService;

    @PostMapping("/insertLog")
    public ResponseBean insertLog(@RequestBody SysLog sysLog) {
        if (sysLogService.insertLog(sysLog)) {
            return ResponseBean.success();
        }else {
            return ResponseBean.error("操作失败");
        }
    }
}

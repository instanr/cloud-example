package com.instanr.syslog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.syslogprovider.db.SysLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {
    int insertLog(@Param("log") SysLog log);
}

package com.instanr.syslog.service.impl;

import com.instanr.syslog.mapper.SysLogMapper;
import com.instanr.syslog.service.SysLogService;
import com.instanr.syslogprovider.db.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public boolean insertLog(SysLog sysLog) {
        int insert = sysLogMapper.insertLog(sysLog);
        return insert == 1;
    }
}

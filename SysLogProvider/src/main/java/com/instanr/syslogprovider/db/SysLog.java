package com.instanr.syslogprovider.db;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/***
 * 系统日志表
 */
@Data
public class SysLog  implements Serializable {

    private static final long serialVersionUID = 1L;

    /***
     * id
     */
    private Long id;
    /***
     * 请求Url
     */
    private String url;

    /***
     * 参数Json
     */
    private String params;

    /***
     * 返回值json
     */
    private String returns;

    /***
     * 执行时长
     */
    private Long execution;

    /***
     * 请求时间
     */
    private LocalDateTime requestTime;

    /***
     * 返回时间
     */
    private LocalDateTime responseTime;

    /***
     * 发起请求的Ip
     */
    private String ip;

    /***
     * 发起请求的地址
     */
    private String location;

    /***
     * 操作人
     */
    private String operator;

    /***
     * 操作人Id
     */
    private Long operatorId;

}

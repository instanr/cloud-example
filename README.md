# Spring Cloud 基础模板框架
每个模块下面都有README

## [Cache 模块](Cache/README.md)

## [Common 模块](Common/README.md)

## [Eureka 模块](Eureka/README.md)

## [Gateway 模块](Gateway/README.md)

## [Users 模块](User/README.md)

## [SysLog 模块](SysLog/README.md)

## 启动顺序
1. Eureka
2. Cache
3. Gateway
4. 剩余模块不分顺序


## 集群使用方法：
只需要在再部署相同的服务即可，需要在eureka 和 gateway 的yml配置文件中修改以下部分:
```yaml
eureka:
  client:
    register-with-eureka: false
    fetch-registry: false
    service-url:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka,http://192.168.3.27:1559/eureka
```
在defaultZone处加上另外一套Eureka的地址即可
package com.instanr.usersprovider.db;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SysDepartment {

    /***
     * id
     */
    private Long id;

    /***
     * 名称
     */
    private String name;

    /***
     * 部门编码
     */
    private String code;

    /***
     * 父级id
     */
    private Long parentId;

    /***
     * 简述
     */
    private String description;

    /***
     * 创建时间
     */
    private LocalDateTime createTime;

    /***
     * 修改时间
     */
    private LocalDateTime modifyTime;

    /***
     * 创建人
     */
    private Long createUser;

    /***
     * 修改人
     */
    private Long modifyUser;

    /***
     * 删除标记
     */
    private Integer delFlag;
}

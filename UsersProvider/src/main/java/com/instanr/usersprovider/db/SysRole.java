package com.instanr.usersprovider.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/***
 * 系统角色表
 * @author instanr
 * @since 2021-10-25
 */
@Data
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /***
     * 角色名
     */
    private String roleName;

    /***
     * 备注
     */
    private String remark;

    /***
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /***
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifyTime;

    /***
     * 创建人
     */
    private Long createUser;

    /***
     * 修改人
     */
    private Long modifyUser;

    /***
     * 是否禁用 1正常， 0禁用
     */
    private Integer status;

    /***
     * 删除标记 0未被删除， 1已删除
     */
    private Integer delFlag;
}

package com.instanr.usersprovider.db;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/***
 * 系统菜单表
 * @author instanr
 * @since 2021-10-25
 */
@Data
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /***
     * id
     */
    private Long id;

    /***
     * 父节点Id
     */
    private Long parentId;

    /***
     * 菜单/按钮名称
     */
    private String menuName;

    /***
     * 菜单的url
     */
    private String url;

    /***
     * 页面的路径
     */
    private String pagePath;

    /***
     * 菜单类型 0：菜单， 1：按钮
     */
    private Integer type;

    /***
     * 菜单/按钮图标
     */
    private String icon;

    /***
     * 排序
     */
    private Integer orderNum;

    /***
     * 菜单描述
     */
    private String description;

    /***
     * 按钮权限编码
     * 只有按钮，这个字段才有值
     */
    private String permission;

    /***
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /***
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifyTime;

    /***
     * 创建时间
     */
    private Long createUser;

    /***
     * 修改时间
     */
    private Long modifyUser;

    /***
     * 是否可用
     */
    private Integer available;

    /***
     * 删除标记
     */
    private Integer delFlag;

}

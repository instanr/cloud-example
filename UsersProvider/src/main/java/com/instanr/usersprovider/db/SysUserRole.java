package com.instanr.usersprovider.db;

import lombok.Data;

import java.io.Serializable;


/***
 * 用户-角色表
 */
@Data
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userId;

    private Long roleId;
}

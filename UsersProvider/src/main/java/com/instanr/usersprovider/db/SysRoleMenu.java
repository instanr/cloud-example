package com.instanr.usersprovider.db;

import lombok.Data;

import java.io.Serializable;

/***
 * 角色-菜单对应表
 * @author instanr
 * @since 2021-10-25
 */
@Data
public class SysRoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long roleId;
    private Long menuId;
}

package com.instanr.usersprovider.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;


/***
 * 系统用户表
 * @author instanr
 * @since 2021-10-22
 */
@Data
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /***
     * 用户ID
     */
    private Long id;
    /***
     * 用户名
     */
    private String username;
    /***
     * 昵称
     */
    private String nickname;
    /***
     * 邮箱
     */
    private String email;
    /***
     * 头像
     */
    private String avatar;
    /***
     * 联系电话
     */
    private String phoneNumber;
    /***
     * 状态 0锁定 1有效
     */
    private Integer status;

    /***
     * 性别 0女 1男 2保密
     */
    private Integer sex;
    /***
     * 盐
     */
    private String salt;
    /***
     * 密码
     */
    private String password;
    /***
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birth;
    /***
     * 部门id
     */
    private Long departmentId;
    /***
     * 删除标记
     */
    private Integer delFlag;

    /***
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /***
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifyTime;

    /***
     * 创建人
     */
    private Long createUser;

    /***
     * 修改人
     */
    private Long modifyUser;
}

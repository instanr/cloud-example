package com.instanr.usersprovider.vo;

import com.instanr.usersprovider.db.SysMenu;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
public class MenuVO extends SysMenu {
    private List<String> roleIds;
    private List<String> roleNames;
}

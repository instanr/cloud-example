package com.instanr.usersprovider.vo;

import com.instanr.usersprovider.db.SysRoleMenu;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleMenuVO extends SysRoleMenu {
    private String menuUrl;
    private String roleName;
}

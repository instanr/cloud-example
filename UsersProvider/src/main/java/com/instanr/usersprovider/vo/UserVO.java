package com.instanr.usersprovider.vo;

import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.db.SysUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserVO extends SysUser {
    /***
     * 机构名称
     */
    private String departmentName;

    /***
     * 角色列表
     */
    private List<SysRole> roles;

    /***
     * 菜单列表
     */
    private List<SysMenu> menus;

    /***
     * 修改密码时用的旧密码
     */
    private String oldPassword;

}

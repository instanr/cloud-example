package com.instanr.usersprovider.vo;

import com.instanr.usersprovider.db.SysRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RoleVO extends SysRole {
    private String createUsername;
    private String modifyUsername;
}

# Gateway 服务
这个服务是中心网关，请求其他服务都应该先通过这个服务的地址来请求，

这个服务附带了 SpringSecurity 作为权限验证处理器

因为 SpringSecurity 的登录和回话管理是用session进行管理，所以需要前端页面允许向后台传输Cookie

## SwaggerUI 的地址为 http://ip:port/doc.html

## 注意大坑
ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator();

在 filter 中如果需要操作返回值，就肯定需要用到上面的这个对象，大体逻辑在代码里面写了，这里就不过多叙述，
需要注意的是: 这个对象不管在多少个filter中、new多少次，都是同一个，他们指向的地址都是同一个，
所以在我们使用这个类里面的方法的时候不能重写只能重载。具体如下演示
```
    ServerHttpResponseDecorator decorator = new ServerHttpResponseDecorator(originalResponse) {
        
        @Override
        public @NotNull Mono<Void> writeWith(@NotNull Publisher<? extends DataBuffer> body) {
            xxxxxxx
            xxxxx
            xxxxxxxxxx
        }
    }
```
在这些重载的方法里面只有你的Filter的Order值为负数（小于0）时才会生效

在这些重载的方法里面只有你的Filter的Order值为负数（小于0）时才会生效

在这些重载的方法里面只有你的Filter的Order值为负数（小于0）时才会生效

重要的事情说三遍
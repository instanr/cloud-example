package com.instanr.gateway.filter;

import com.google.common.base.Throwables;
import com.instanr.common.utils.JWTUtil;
import com.instanr.gateway.feign.SysLogFeign;
import com.instanr.gateway.threads.SysLogThread;
import com.instanr.gateway.util.AddressUtil;
import com.instanr.gateway.util.FilterUtil;
import com.instanr.syslogprovider.db.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.CachedBodyOutputMessage;
import org.springframework.cloud.gateway.support.BodyInserterContext;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.http.codec.HttpMessageReader;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/***
 * 系统日志过滤器
 * @author instanr
 * @since 2022-04-29
 */
@Slf4j
@Component
public class SysLogFilter implements GlobalFilter, Ordered {

    @Autowired
    private SysLogFeign sysLogFeign;

    private final List<HttpMessageReader<?>> messageReaders = HandlerStrategies.withDefaults().messageReaders();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();
        // 请求路径
        String requestPath = request.getPath().pathWithinApplication().value();
        // 获取Ip地址 这一步在本地测试会比较慢，部署到线上就会快很多(只要不是本地请求都会很快)
        String ipAddress = AddressUtil.getIP(request);
        SysLog sysLog = new SysLog();
        sysLog.setRequestTime(LocalDateTime.now());
        sysLog.setIp(ipAddress);
        // 解算地理位置被放在了线程里面单独执行，因为耗时太长
        // sysLog.setLocation(AddressUtil.getCityInfo(ipAddress));
        sysLog.setUrl(requestPath);
        List<String> authorization = request.getHeaders().get("Authorization");
        if (authorization != null && authorization.size() > 0) {
            String[] username = JWTUtil.getUsername(authorization.get(0));
            sysLog.setOperator(username[0]);
            sysLog.setOperatorId(Long.parseLong(username[1]));
        }

        MediaType mediaType = request.getHeaders().getContentType();
        if (mediaType != null && (MediaType.APPLICATION_FORM_URLENCODED.isCompatibleWith(mediaType) || MediaType.APPLICATION_JSON.isCompatibleWith(mediaType))) {
            return writeBodyLog(exchange, chain, sysLog);
        } else {
            return writeBasicLog(exchange, chain, sysLog);
        }
    }

    /***
     * 将 UrlParams 写入 SysLog 中
     * @param exchange
     * @param chain
     * @param sysLog
     * @return
     */
    private Mono<Void> writeBasicLog(ServerWebExchange exchange, GatewayFilterChain chain, SysLog sysLog) {
        MultiValueMap<String, String> queryParams = exchange.getRequest().getQueryParams();
        sysLog.setParams(getUrlParamsByMap(queryParams));
        return returnMono(chain, exchange, sysLog);
    }


    /**
     * 将 Body 中的参数写入 SysLog 中
     * 解决 request body 只能读取一次问题
     * @param exchange
     * @param chain
     * @param sysLog
     * @return
     */
    private Mono<Void> writeBodyLog(ServerWebExchange exchange, GatewayFilterChain chain, SysLog sysLog) {
        ServerRequest serverRequest = ServerRequest.create(exchange, messageReaders);

        Mono<String> modifiedBody = serverRequest.bodyToMono(String.class).flatMap(body -> {
            sysLog.setParams(body);
            return Mono.just(body);
        });

        // 通过 BodyInserter 插入 body(支持修改body), 避免 request body 只能获取一次
        BodyInserter<Mono<String>, ReactiveHttpOutputMessage> bodyInserter = BodyInserters.fromPublisher(modifiedBody, String.class);
        HttpHeaders headers = new HttpHeaders();
        headers.putAll(exchange.getRequest().getHeaders());
        CachedBodyOutputMessage outputMessage = new CachedBodyOutputMessage(exchange, headers);
        return bodyInserter.insert(outputMessage, new BodyInserterContext()).then(Mono.defer(() -> {
            ServerHttpRequest decorator = FilterUtil.decorate(exchange, headers, outputMessage);
            return returnMono(chain, exchange.mutate().request(decorator).build(), sysLog);
        }));
    }

    /***
     * 在这里记录日志并返回
     * @param chain {@link GatewayFilterChain}
     * @param exchange {@link ServerWebExchange}
     * @return {@link Mono} 返回体
     */
    private Mono<Void> returnMono(GatewayFilterChain chain,ServerWebExchange exchange, SysLog sysLog){

        ServerHttpResponse originalResponse = exchange.getResponse();
        DataBufferFactory bufferFactory = originalResponse.bufferFactory();
        // 注意：这个对象下面的重写只有在 getOrder() 返回负数时才会生效
        ServerHttpResponseDecorator decorator = new ServerHttpResponseDecorator(originalResponse) {

            @Override
            public @NotNull Mono<Void> writeWith(@NotNull Publisher<? extends DataBuffer> body) {
                if (body instanceof Flux) {
                    Flux<? extends DataBuffer> fluxBody = Flux.from(body);
                    // 这里跟 RequestParamsFilter 里面的处理不太一样
                    // 主要是因为这里是防止返回体过长被截取的情况
                    // 又因为这个 filter 的 Order 在 RequestParamsFilter 之前
                    // 所以这里就先行处理过长返回体，这里处理之后下一个返回体就不用在做处理了
                    return super.writeWith(fluxBody.buffer().map(dataBuffers -> {
                        List<String> list = new ArrayList<String>();
                        dataBuffers.forEach(dataBuffer -> {
                            try {
                                byte[] content = new byte[dataBuffer.readableByteCount()];
                                dataBuffer.read(content);
                                DataBufferUtils.release(dataBuffer);
                                list.add(new String(content, StandardCharsets.UTF_8));
                            } catch (Exception e) {
                                log.info("加载Response字节流异常，失败原因：{}", Throwables.getStackTraceAsString(e));
                            }
                        });
                        StringBuilder sb = new StringBuilder();
                        list.forEach(sb::append);
                        LocalDateTime responseTime = LocalDateTime.now();
                        long l = Duration.between(sysLog.getRequestTime(), responseTime).toMillis();
                        sysLog.setResponseTime(responseTime);
                        sysLog.setExecution(l);
                        sysLog.setReturns(sb.toString());
                        byte[] uppedContent = sb.toString().getBytes();
                        return bufferFactory.wrap(uppedContent);
                    }));
                }
                return super.writeWith(body);
            }
        };
        return chain.filter(exchange.mutate().response(decorator).build()).then(Mono.fromRunnable(() -> {
            SysLogThread sysLogThread = new SysLogThread(sysLog, sysLogFeign);
            sysLogThread.start();
        }));
    }

    /**
     * 将map参数转换成url参数
     *
     * @param map
     * @return
     */
    private String getUrlParamsByMap(MultiValueMap<String, String> map) {
        if (ObjectUtils.isEmpty(map)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue().get(0));
            sb.append("&");
        }
        String s = sb.toString();
        if (s.endsWith("&")) {
            s = StringUtils.substringBeforeLast(s, "&");
        }
        return s;
    }

    @Override
    public int getOrder() {
        return -2;
    }
}

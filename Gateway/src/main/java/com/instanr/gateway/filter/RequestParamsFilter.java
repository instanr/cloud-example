package com.instanr.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.instanr.common.beans.ResponseBean;
import com.instanr.common.exception.RunningException;
import com.instanr.common.utils.AesUtil;
import com.instanr.gateway.config.GatewayProperties;
import com.instanr.gateway.util.FilterUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.CachedBodyOutputMessage;
import org.springframework.cloud.gateway.support.BodyInserterContext;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;

/***
 * 这里是参数处理的过滤器
 * <p>
 * 包括 <b>请求体参数解密</b> 和 <b>返回体数据加密</b>
 * </p>
 * @author instanr
 * @since 2022-04-08
 */
@Slf4j
@Component
public class RequestParamsFilter implements GlobalFilter, Ordered {

    @Value("${request-key}")
    private String key;

    /***
     *
     * 这里主要分两种方式Get方式和非Get方式，非Get方式的请求数据一般放在requestBody中
     * <p>
     *
     * <b style="color: red;">但是 </b>
     * 因为Flux的特殊性，在非Get的请求中需要同步获取请求体中的数据，这里的办法是重新构造一个请求体({@link ServerRequest})
     * <br>
     * 这里只写了json方式传输数据的解析，如果有其他的方式（如form），请在下面这行 if 后面加其他的方式
     * </p>
     * <h4>
     * if (MediaType.APPLICATION_JSON.isCompatibleWith(mediaType))
     * </h4>
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        String requestPath = request.getPath().pathWithinApplication().value();
        if (Arrays.asList(GatewayProperties.urlWithoutEncrypt).contains(requestPath)) {
            return chain.filter(exchange);
        }

        MediaType mediaType = request.getHeaders().getContentType();
        if(mediaType != null && (MediaType.APPLICATION_FORM_URLENCODED.isCompatibleWith(mediaType) || MediaType.APPLICATION_JSON.isCompatibleWith(mediaType))){
            // 重新构造request，参考ModifyRequestBodyGatewayFilterFactory
            ServerRequest serverRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders());
            // 在这里同步获取加密的数据并解密，再返回解密的数据出来
            Mono<String> modifiedBody = serverRequest.bodyToMono(String.class).flatMap(body -> {
                // 只获取格式为Json的数据因为加密的数据都保存在jsonBody里面
                if (MediaType.APPLICATION_JSON.isCompatibleWith(mediaType)) {
                    JSONObject jsonObject = JSONObject.parseObject(body);
                    String paramStr = jsonObject.getString("data");
                    String newBody;

                    try{
                        newBody = verifySignature(paramStr).toJSONString();
                    }catch (Exception e){
                        String errorStr = "解密数据失败";
                        log.error(errorStr);
                        return Mono.error(new RunningException(errorStr));
                    }
                    return Mono.just(newBody);
                }
                return Mono.empty();
            });
            BodyInserter<Mono<String>, ReactiveHttpOutputMessage> bodyInserter = BodyInserters.fromPublisher(modifiedBody, String.class);
            HttpHeaders headers = new HttpHeaders();
            headers.putAll(request.getHeaders());
            // 这里移除ContentLength是因为解密后的数据长度和原始数据长度不一致，再去请求子模块的接口时会报400
            headers.remove("Content-Length");
            CachedBodyOutputMessage outputMessage = new CachedBodyOutputMessage(exchange, headers);
            return bodyInserter.insert(outputMessage, new BodyInserterContext()).then(Mono.defer(() -> {
                ServerHttpRequest decorator = FilterUtil.decorate(exchange, headers, outputMessage);
                return returnMono(chain, exchange.mutate().request(decorator).build());
            }));
        }else {
            MultiValueMapAdapter<String, String> params = (MultiValueMapAdapter<String, String>) request.getQueryParams();
            ServerHttpRequest newRequest = null;
            if (!CollectionUtils.isEmpty(params)) {
                String paramStr = params.getFirst("data");
                try{
                    if (StringUtils.isNotBlank(paramStr)) {
                        JSONObject trueParam = verifySignature(paramStr);
                        URI uri = request.getURI();
                        String rawQuery = uri.getRawQuery();
                        System.out.println(rawQuery);
                        StringBuilder query = new StringBuilder();
                        trueParam.keySet().forEach(key -> {
                            String value;
                            if ("null".equals(trueParam.getString(key)) || trueParam.getString(key) == null) {
                                value = "";
                            }else {
                                value = trueParam.getString(key);
                            }
                            query.append(key).append("=").append(value).append("&");
                        });
                        URI newUri = UriComponentsBuilder.fromUri(uri).replaceQuery(query.toString()).build(true).toUri();
                        newRequest = exchange.getRequest().mutate().uri(newUri).build();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    String errorStr = "解密数据失败";
                    log.error(errorStr);
                    return Mono.error(new RunningException(errorStr));
                }
            }
            if (newRequest == null) {
                newRequest = request;
            }
            return returnMono(chain, exchange.mutate().request(newRequest).build());
        }
    }

    /***
     * 在这里加密返回值并直接传回数据
     * @param chain {@link GatewayFilterChain}
     * @param exchange {@link ServerWebExchange}
     * @return {@link Mono} 返回体
     */
    private Mono<Void> returnMono(GatewayFilterChain chain,ServerWebExchange exchange){

        ServerHttpResponse originalResponse = exchange.getResponse();
        DataBufferFactory bufferFactory = originalResponse.bufferFactory();
        // 这里是创建新的Response返回回去
        ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator(originalResponse) {
            @Override
            public @NotNull Mono<Void> writeWith(@NotNull Publisher<? extends DataBuffer> body) {
                if (body instanceof Flux) {
                    Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>) body;
                    return super.writeWith(fluxBody.map(dataBuffer -> {
                        // probably should reuse buffers
                        byte[] content = new byte[dataBuffer.readableByteCount()];
                        dataBuffer.read(content);
                        String respStr = new String(content, StandardCharsets.UTF_8);
                        //释放掉内存
                        //DataBufferUtils.release(dataBuffer);
                        String lastStr = "";
                        System.out.println(respStr);
                        //修改response之后的字符串
                        try {
                            ResponseBean responseBean = JSON.toJavaObject(JSON.parseObject(respStr), ResponseBean.class);
                            String dataStr = JSON.toJSONString(responseBean.getData());
                            String encrypt;
                            if (ObjectUtils.isEmpty(dataStr) || "null".equals(dataStr)) {
                                encrypt = null;
                            }else {
                                encrypt = AesUtil.encrypt(dataStr, key);
                            }
                            responseBean.setData(encrypt);
                            lastStr = responseBean.toString();
                        } catch (Exception e) {
                            throw new RuntimeException("返回数据时失败");
                        }
                        byte[] uppedContent = lastStr.getBytes();
                        return bufferFactory.wrap(uppedContent);
                    }));
                }
                return super.writeWith(body);
            }
        };
        return chain.filter(exchange.mutate().response(decoratedResponse).build());

    }

    /***
     * 在这里解密数据以及其他的操作
     * @param paramStr 密文
     * @return 解密后的json数据
     * @throws Exception 解密时的错误
     */
    private JSONObject verifySignature(String paramStr) throws Exception{
        String decrypt = AesUtil.decrypt(paramStr, key);
        return JSONObject.parseObject(decrypt);
    }

    @Override
    public int getOrder() {
        return -3;
    }
}

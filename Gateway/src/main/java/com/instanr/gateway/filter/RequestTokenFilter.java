package com.instanr.gateway.filter;

import com.instanr.common.beans.RedisSaveModel;
import com.instanr.common.beans.ResponseBean;
import com.instanr.common.consts.SysConstants;
import com.instanr.common.eunm.ErrorCodeEnum;
import com.instanr.common.utils.JWTUtil;
import com.instanr.common.utils.ObjectUtil;
import com.instanr.gateway.config.GatewayProperties;
import com.instanr.gateway.feign.CacheFeign;
import com.instanr.usersprovider.db.SysUser;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/***
 * 全局过滤器，用于过滤是否含有token
 * @author instanr
 * @since 2022-02-16
 */
@Slf4j
@Component
@AllArgsConstructor
public class RequestTokenFilter implements GlobalFilter, Ordered {

    private CacheFeign cacheFeign;

    /***
     * 过滤器内容,在这里过滤Token
     * @param exchange 请求体
     * @param chain 过滤器主体
     * @return 返回
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();
        String methodValue = request.getMethodValue();
        String path = request.getURI().getPath();
        log.info("收到 " + methodValue + " 请求："+ path);
        // 获取token
        List<String> authorization = request.getHeaders().get("Authorization");
        boolean shouldFilter = false;
        String errorMsg = "";
        if (!Arrays.asList(GatewayProperties.excludedAuthPages).contains(path)) {
            // 没有获取到 Token
            if (authorization == null || authorization.size() == 0) {
                shouldFilter = true;
                errorMsg = "禁止访问，没有有效token！";
            } else {
                String token = authorization.get(0);
                // 验证token是否过期
                if (JWTUtil.isExpire(token)) {
                    shouldFilter = true;
                    errorMsg = "禁止访问，token已经失效！";
                } else {
                    // 验证token有效性（拒绝多边登录）
                    String[] jwtUsername = JWTUtil.getUsername(token);
                    String username = jwtUsername[0];
                    String verifyUsername = jwtUsername[0] + "-" + jwtUsername[1];
                    RedisSaveModel redisSaveModel = new RedisSaveModel();
                    redisSaveModel.setKey(SysConstants.USER_LOGIN_REDIS_KEY_HEAD + username);
                    RedisSaveModel userCache = cacheFeign.getData(redisSaveModel);
                    Map<String, Object> cacheData = (Map<String, Object>) userCache.getData();
                    SysUser userInfo = ObjectUtil.objectToJavaObject(cacheData.get("userInfo"), SysUser.class);
                    String oldToken = (String) cacheData.get("token");
                    if (oldToken.equals(token)) {
                        Boolean verify = JWTUtil.verify(verifyUsername, token, userInfo.getPassword());
                        if (!verify) {
                            shouldFilter = true;
                            errorMsg = "禁止访问，token验证不通过";
                        }
                    }else {
                        shouldFilter = true;
                        errorMsg = "禁止访问，使用了非正常的token";
                    }
                }
            }

            if (shouldFilter) {
                ServerHttpResponse response = exchange.getResponse();
                // 修改返回体
                byte[] respBytes = ResponseBean.error(ErrorCodeEnum.FORBIDDEN.getResultCode(), errorMsg).toString().getBytes();
                DataBuffer wrap = response.bufferFactory().wrap(respBytes);
                return response.writeWith(Mono.just(wrap));
            }
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -1;
    }
}

package com.instanr.gateway.config;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

/***
 * 通用参数配置
 * <h2>注意Gateway中存在一个同名包{@link org.springframework.cloud.gateway.config.GatewayProperties}</h2>
 * 在使用的时候注意不要引用错误
 */
public class GatewayProperties {

    /***
     * security的鉴权排除的url列表
     */
    public static final String[] excludedAuthPages = {
            "/login",

            // swagger
            "/doc.html",
            "/webjars/**",
            "/favicon.ico",
            "/swagger-resources",
            // 用户模块
            "/users/v2/api-docs",
            // Cache模块
            "/cache/v2/api-docs",
            // Log模块
            "/syslog/v2/api-docs",
    };

    // 跳过加密解密模块的请求地址
    public static final String[] urlWithoutEncrypt = {
            // swagger
            "/doc.html",
            "/webjars/**",
            "/favicon.ico",
            "/swagger-resources",
            // 用户模块
            "/users/v2/api-docs",
            // Cache模块
            "/cache/v2/api-docs",
            // Log模块
            "/syslog/v2/api-docs",
    };

    /***
     *
     * @return
     */
    public static UrlBasedCorsConfigurationSource getCorsConfigSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedMethod("*");
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}

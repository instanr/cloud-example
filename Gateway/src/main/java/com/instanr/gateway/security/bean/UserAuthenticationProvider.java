package com.instanr.gateway.security.bean;

import com.instanr.common.beans.ResponseBean;
import com.instanr.common.utils.MD5Util;
import com.instanr.gateway.feign.UsersFeign;
import com.instanr.usersprovider.db.SysUser;
import com.instanr.usersprovider.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/***
 * 验证核心
 * 在这里验证用户名和密码
 * @author instanr
 * @since 2022-03-23
 */
@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    /***
     * Users组件的API
     */
    @Autowired
    UsersFeign usersFeign;

    /***
     * 在这里验证用户名和密码
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取前端表单中输入后返回的用户名、密码
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        // 在这里往数据库中取出用户信息
        ResponseBean userResp = usersFeign.getUserInfo(username);
        if (userResp.getCode() != 200) {
            throw new UsernameNotFoundException("获取用户失败：" + userResp.getMsg());
        }
        UserVO user = userResp.toJavaObject(UserVO.class);
        if (user == null) {
            throw new UsernameNotFoundException("未找到用户");
        }
        SecurityUser userInfo = new SecurityUser(user);
        // 验证密码
        String md5Pass = MD5Util.md5Encryption(password, user.getSalt());
        if (!md5Pass.equals(user.getPassword())) {
            throw new BadCredentialsException("密码错误！");
        }

        if (user.getStatus() == 0) {
            throw new LockedException("此账号被锁定");
        }
        // 前后端分离情况下 处理逻辑...
        // 更新登录令牌 - 之后访问系统其它接口直接通过token认证用户权限...
        // 在这里将User信息写入SecurityUser中
        return new UsernamePasswordAuthenticationToken(userInfo, password, userInfo.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}

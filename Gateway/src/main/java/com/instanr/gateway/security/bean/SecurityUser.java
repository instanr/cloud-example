package com.instanr.gateway.security.bean;

import com.instanr.usersprovider.db.SysUser;
import com.instanr.usersprovider.vo.UserVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/***
 * SpringSecurity 内部保存的当前登录用户对象
 * 这个对象会被保存到{@link javax.ws.rs.core.SecurityContext SecurityContext}对象中
 * @author instanr
 * @since 2022-03-25
 */
@Data
public class SecurityUser implements UserDetails {
    /**
     * 当前登录用户
     */
    private transient UserVO currentUser;

    public SecurityUser() {
    }

    public SecurityUser(UserVO user) {
        if (user != null) {
            this.currentUser = user;
        }
    }

    public UserVO getCurrentUser() {
        return this.currentUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        currentUser.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return currentUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.currentUser.getStatus() == 1;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.currentUser.getStatus() == 1;
    }
}

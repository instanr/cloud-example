package com.instanr.gateway.security.handler;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.instanr.common.beans.RedisSaveModel;
import com.instanr.common.beans.ResponseBean;
import com.instanr.common.consts.SysConstants;
import com.instanr.common.eunm.ErrorCodeEnum;
import com.instanr.common.utils.JWTUtil;
import com.instanr.gateway.feign.CacheFeign;
import com.instanr.gateway.security.bean.SecurityUser;
import com.instanr.usersprovider.db.SysUser;
import com.instanr.usersprovider.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/***
 * 自定义登录成功处理
 */
@Component
public class AdminAuthenticationSuccessHandler implements ServerAuthenticationSuccessHandler {

    @Value("${jwt-token.expire-time}")
    private Long tokenExpireTime;

    @Autowired
    CacheFeign cacheFeign;

    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
        ServerWebExchange exchange = webFilterExchange.getExchange();
        ServerHttpResponse response = exchange.getResponse();
        //设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        //设置body
        byte[] dataBytes = {};
        try {
            SecurityUser user = (SecurityUser) authentication.getPrincipal();
            UserVO currentUser = user.getCurrentUser();
            // 可以在这里生成Jwt
            String jwtUsername = currentUser.getUsername() + "-" +currentUser.getId();
            String token = JWTUtil.sign(jwtUsername, currentUser.getPassword(), tokenExpireTime);
            JSONObject data = new JSONObject(){{
                put("token", token);
                put("userInfo", currentUser);
            }};
            ResponseBean success = ResponseBean.success(data);
            dataBytes = success.toString().getBytes(StandardCharsets.UTF_8);

            RedisSaveModel redisData = new RedisSaveModel(
                    SysConstants.USER_LOGIN_REDIS_KEY_HEAD + currentUser.getUsername(),
                    data,
                    tokenExpireTime
            );
            boolean b = cacheFeign.redisSave(redisData);
            if (!b) {
                throw new Exception();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ResponseBean responseBean = ResponseBean.error(
                    ErrorCodeEnum.INTERNAL_SERVER_ERROR.getResultCode(),
                    "授权失败"
            );
            dataBytes = responseBean.toString().getBytes(StandardCharsets.UTF_8);
        }
        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(dataBytes);
        return response.writeWith(Mono.just(bodyDataBuffer));
    }
}

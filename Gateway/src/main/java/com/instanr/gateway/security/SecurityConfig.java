package com.instanr.gateway.security;

import com.instanr.gateway.config.GatewayProperties;
import com.instanr.gateway.security.handler.UserAccessDeniedHandler;
import com.instanr.gateway.security.manage.AccessAuthorizationManager;
import com.instanr.gateway.security.manage.UserAuthenticationManager;
import com.instanr.gateway.security.handler.AdminAuthenticationFailureHandler;
import com.instanr.gateway.security.handler.AdminAuthenticationSuccessHandler;
import com.instanr.gateway.security.handler.CustomHttpBasicServerAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/***
 * SpringSecurity 配置类
 * @author instanr
 * @since 2022-03-23
 */
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    /***
     * 登录成功后的处理
     */
    @Autowired
    AdminAuthenticationSuccessHandler successHandler;

    /***
     * 登录失败后的处理
     */
    @Autowired
    AdminAuthenticationFailureHandler failureHandler;

    /***
     * 登录后续请求鉴权失败的处理
     */
    @Autowired
    CustomHttpBasicServerAuthenticationEntryPoint customHttpBasicServerAuthenticationEntryPoint;

    /***
     * 登录验证处理器
     */
    @Autowired
    UserAuthenticationManager authenticationManager;

    /***
     * 接口权限处理器
     */
    @Autowired
    AccessAuthorizationManager accessAuthorizationManager;

    /***
     * 登录被拒绝后的处理器
     */
    @Autowired
    UserAccessDeniedHandler accessDeniedHandler;

    @Bean
    public SecurityWebFilterChain webFluxSecurityFilterChain(ServerHttpSecurity http) {

        http
                .authorizeExchange()
                // 无需进行权限过滤的请求路径
                .pathMatchers(GatewayProperties.excludedAuthPages).permitAll()
                // option 请求默认放行
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                // 设置接口权限处理器
                .anyExchange().access(accessAuthorizationManager)
                .and().httpBasic()
                .and()
                // 设置登录请求的路径
                .formLogin().loginPage("/login")
                // TODO 后续再测试其用处
                // .securityContextRepository()
                // 登录认证处理器
                .authenticationManager(authenticationManager)
                // 认证成功
                .authenticationSuccessHandler(successHandler)
                // 登陆验证失败
                .authenticationFailureHandler(failureHandler)
                // 基于http的接口请求鉴权失败
                .and().exceptionHandling().authenticationEntryPoint(customHttpBasicServerAuthenticationEntryPoint)
                // 鉴权失败（接口不允许访问）处理
                .accessDeniedHandler(accessDeniedHandler)
                // 支持跨域
                .and().csrf().disable().cors().configurationSource(GatewayProperties.getCorsConfigSource())
                .and().logout().disable();

        return http.build();
    }

}

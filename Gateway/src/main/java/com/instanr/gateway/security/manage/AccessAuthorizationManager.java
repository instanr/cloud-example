package com.instanr.gateway.security.manage;

import com.instanr.common.beans.RedisSaveModel;
import com.instanr.gateway.feign.CacheFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


/***
 * <h1>接口校验处理器</h1>
 * <h2 style="color: red;">处理逻辑</h2>
 *
 * 登录的用户会将用户信息存在{@link com.instanr.gateway.security.bean.SecurityUser SecurityUser}中,
 * 这个方法实现了{@link org.springframework.security.core.userdetails.UserDetails UserDetails} 这个接口<br>
 * 这个类有点像 Shiro 的 ActiveUser， 会根据当前请求的 SessionId 拿到改请求对应的 SecurityUser<br>
 * 这个类会被 SpringSecurity 保存在{@link org.springframework.security.core.context.SecurityContext SecurityContext} 中
 * <br><br>
 * <h3 style="color: red;">重点来了</h3>
 * {@link ReactiveAuthorizationManager} 实现这个接口，然后重写 check 方法<br>
 * 方法中 <strong>return</strong> 后面的一串会将穿进去的字符串列表（List<String>）拿去跟 SecurityUser中的 Authorities 字段做比较<br>
 * 如果有相同的字符串就放行，没有就拦截
 */
@Component
public class AccessAuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext>{

    @Autowired
    private CacheFeign cacheFeign;

    /***
     * 权限决策处理
     * @param authentication 携带当前登录的用户
     * @param authorizationContext 请求上下文，携带请求体
     * @return 是否允许
     */
    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> authentication, AuthorizationContext authorizationContext) {
        //从Redis中获取当前路径可访问角色列表
        URI uri = authorizationContext.getExchange().getRequest().getURI();
        RedisSaveModel menusRoleList = null;
        try {
            menusRoleList = cacheFeign.getHasData(new RedisSaveModel("menusRoleList", uri.getPath()));
        }catch (Exception e) {
            throw new RuntimeException("获取登录状态失败");
        }
        List<String> authorities = (List<String>)menusRoleList.getData();
        // 如果没有请求到该请求地址的相关权限，则直接放行
        if (authorities == null) {
            return Mono.just(new AuthorizationDecision(true));
        }
        //认证通过且角色匹配的用户可访问当前路径
        return authentication
                .filter(Authentication::isAuthenticated)
                .flatMapIterable(Authentication::getAuthorities)
                .map(GrantedAuthority::getAuthority)
                .any(authorities::contains)
                .map(AuthorizationDecision::new)
                .defaultIfEmpty(new AuthorizationDecision(true));
    }
}

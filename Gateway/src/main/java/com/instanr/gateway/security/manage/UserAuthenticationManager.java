package com.instanr.gateway.security.manage;

import com.instanr.gateway.security.bean.UserAuthenticationProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Objects;

/***
 * 用户登录验证处理器
 * @author instanr
 * @since 2022-03-23
 */
@Component
public class UserAuthenticationManager implements ReactiveAuthenticationManager {

    /***
     * 验证提供者（验证核心）
     */
    private final UserAuthenticationProvider adminAuthenticationProvider;

    public UserAuthenticationManager(UserAuthenticationProvider adminAuthenticationProvider) {
        this.adminAuthenticationProvider = adminAuthenticationProvider;
    }

    /***
     * 在这里调用验证提供者来验证用户
     * @param authentication 包含登录传来的信息（用户名和密码）
     * @return {@link Authentication} 登录鉴权成功后的返回
     * @throws AuthenticationException 验证异常
     */
    @Override
    public Mono<Authentication> authenticate(Authentication authentication) throws AuthenticationException {
        Authentication result = adminAuthenticationProvider.authenticate(authentication);
        if (Objects.nonNull(result)) {
            return Mono.just(result);
        }
        throw new ProviderNotFoundException("网络错误，请稍后再试");
    }
}


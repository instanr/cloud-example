package com.instanr.gateway.feign;

import com.instanr.common.beans.ResponseBean;
import com.instanr.usersprovider.db.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/***
 * Users 模块的内部请求
 * @author instanr
 * @since 2022-02-23
 */
@FeignClient(name = "users")
public interface UsersFeign {

    @GetMapping("/user/getUserInfo")
    ResponseBean getUserInfo(@RequestParam("username") String username);

}

package com.instanr.gateway.feign;

import com.instanr.common.beans.RedisSaveModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/***
 * Cache 模块的内部请求
 * @author instanr
 * @since 2022-02-23
 */
@FeignClient(name = "cache")
public interface CacheFeign {

    @PostMapping("/redis/save")
    boolean redisSave(@RequestBody RedisSaveModel model);

    @PostMapping("/redis/removeKey")
    boolean redisRemoveKey(@RequestBody RedisSaveModel model);

    @PostMapping("/redis/getData")
    RedisSaveModel getData(@RequestBody RedisSaveModel query);

    @PostMapping("/redis/getHasData")
    RedisSaveModel getHasData(@RequestBody RedisSaveModel query);
}

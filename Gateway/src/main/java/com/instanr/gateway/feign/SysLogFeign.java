package com.instanr.gateway.feign;

import com.instanr.common.beans.ResponseBean;
import com.instanr.syslogprovider.db.SysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "syslog")
public interface SysLogFeign {

    @PostMapping("sysLog/insertLog")
    ResponseBean insertLog(@RequestBody SysLog sysLog);
}

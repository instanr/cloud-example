package com.instanr.gateway.threads;

import com.instanr.gateway.feign.SysLogFeign;
import com.instanr.gateway.util.AddressUtil;
import com.instanr.syslogprovider.db.SysLog;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class SysLogThread extends Thread{

    private SysLog sysLog;
    private SysLogFeign feign;

    public SysLogThread(SysLog sysLog, SysLogFeign feign) {
        this.sysLog = sysLog;
        this.feign = feign;
    }

    @Override
    public void run() {
        // 在这里解算地理位置是因为这一步过程用时十分不稳定，会消耗大概5~6秒的时间
        sysLog.setLocation(AddressUtil.getCityInfo(sysLog.getIp()));
        feign.insertLog(sysLog);
        log.info("日志记录成功" + LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE));
    }
}

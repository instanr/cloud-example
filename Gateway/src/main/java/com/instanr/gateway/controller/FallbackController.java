package com.instanr.gateway.controller;

import com.instanr.common.beans.ResponseBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fallback")
public class FallbackController {

    @RequestMapping("/user")
    public ResponseBean userFallback() {
        return ResponseBean.error("Users服务请求响应超时");
    }

    @RequestMapping("/cache")
    public ResponseBean cacheFallback() {
        return ResponseBean.error("Users服务请求响应超时");
    }

    @RequestMapping("/sysLog")
    public ResponseBean sysLogFallback() {
        return ResponseBean.error("Users服务请求响应超时");
    }
}

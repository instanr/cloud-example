package com.instanr.user.service.impl;

import com.instanr.common.beans.PageVO;
import com.instanr.common.beans.RedisSaveModel;
import com.instanr.user.feign.CacheFeign;
import com.instanr.user.mapper.MenuMapper;
import com.instanr.user.service.MenuService;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.vo.MenuVO;
import com.instanr.usersprovider.vo.RoleMenuVO;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private CacheFeign cacheFeign;

    @PostConstruct
    private void init() {
        this.jointMenusRoleList();
    }

    @Override
    public List<SysMenu> getMenuListByRoles(List<SysRole> roles) {
        List<Long> roleIds = new ArrayList<>();
        roles.forEach(item -> {
            roleIds.add(item.getId());
        });
        return menuMapper.getMenuListByIds(roleIds);
    }

    @Override
    public void jointMenusRoleList() {
        List<RoleMenuVO> menuRoleList = menuMapper.getMenuRoleList();
        Map<String, List<String>> menusRoleList = new HashMap<>();
        menuRoleList.forEach(item -> {
            if (menusRoleList.get(item.getMenuUrl()) != null) {
                menusRoleList.get(item.getMenuUrl()).add(item.getRoleName());
            }else {
                List<String> roleNames = new ArrayList<>(){{
                    add(item.getRoleName());
                }};
                menusRoleList.put(item.getMenuUrl(), roleNames);
            }
        });
        cacheFeign.redisHasSave(new RedisSaveModel("menusRoleList", menusRoleList));
    }

    @Override
    public Boolean add(SysMenu menu) {
        return null;
    }

    @Override
    public Boolean update(SysMenu menu) {
        return null;
    }

    @Override
    public boolean delete(SysMenu menu) {
        return false;
    }

    @Override
    public List<MenuVO> getMenuList(SysMenu menu) {
        return null;
    }

    @Override
    public PageVO<MenuVO> getMenuList(SysMenu menu, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public List<MenuVO> getMenuTree(SysMenu menu) {
        return null;
    }
}

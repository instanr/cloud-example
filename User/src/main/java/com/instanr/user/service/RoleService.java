package com.instanr.user.service;


import com.instanr.common.beans.PageVO;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.vo.RoleVO;

import java.util.List;

/***
 * 角色表相关操作
 * @author instanr
 * @since 2021-10-27
 */
public interface RoleService {

    /***
     * 通过用户的Id获取其角色列表
     * @param userId userId
     * @return 角色列表
     */
    List<SysRole> findRolesByUserId(Long userId);

    /***
     * 添加角色
     * @param role 基本信息
     * @return 成功与否
     */
    Boolean add(SysRole role);

    /***
     * 修改角色
     * @param role 基本信息
     * @return 成功与否
     */
    Boolean update(SysRole role);

    /***
     * 删除角色
     * @param role 基本信息
     * @return 成功与否
     */
    Boolean delete(SysRole role);

    /***
     * 设置角色下的菜单列表
     * @param id 角色id
     * @param menus 菜单Id列表
     * @return 成功与否
     */
    Boolean setRoleMenus(Long id, List<Long> menus);

    /***
     * 批量给用户赋权
     * @param id 角色id
     * @param users 用户id列表
     * @return 成功与否
     */
    Boolean giveRoleToUsers(Long id, List<Long> users);

    /***
     * 查询角色列表
     * @param role 查询条件
     * @return 角色列表
     */
    List<RoleVO> getRoleLis(SysRole role);

    /***
     * 查询角色列表（分页）
     * @param role 查询条件
     * @param pageNum 页码
     * @param pageSize 页长
     * @return 角色列表
     */
    PageVO<RoleVO> getRoleLis(SysRole role, Integer pageSize, Integer pageNum);
}

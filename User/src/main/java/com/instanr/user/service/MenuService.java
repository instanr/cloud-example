package com.instanr.user.service;

import com.instanr.common.beans.PageVO;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.vo.MenuVO;

import java.util.List;

public interface MenuService {

    /***
     * 通过角色列表获取菜单列表
     * @param roles 角色列表
     * @return 菜单列表
     */
    List<SysMenu> getMenuListByRoles(List<SysRole> roles);

    /***
     * 拼接menu对应的角色列表，并存入redis
     * 用于SpringSecurity接口校验
     * <br/>
     * # 基本格式
     * {
     *     "url": ["admin", "admin1"]
     *     ....
     * }
     */
    void jointMenusRoleList();

    /***
     * 添加菜单
     * @param menu 菜单信息
     * @return 成功与否
     */
    Boolean add(SysMenu menu);

    /***
     * 修改菜单
     * @param menu 菜单信息
     * @return 成功与否
     */
    Boolean update(SysMenu menu);

    /***
     * 删除菜单
     * @param menu 只带id
     * @return 成功与否
     */
    boolean delete(SysMenu menu);

    /***
     * 查询菜单列表
     * @param menu 查询信息
     * @return 菜单列表
     */
    List<MenuVO> getMenuList(SysMenu menu);

    /***
     * 查询菜单列表（分页）
     * @param menu 菜单信息
     * @param pageSize 页长
     * @param pageNum 页码
     * @return 菜单列表
     */
    PageVO<MenuVO> getMenuList(SysMenu menu, Integer pageSize, Integer pageNum);

    /***
     * 获取菜单树
     * @param menu 查询条件
     * @return 菜单树
     */
    List<MenuVO> getMenuTree(SysMenu menu);
}

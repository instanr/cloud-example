package com.instanr.user.service;


import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.db.SysUser;
import com.instanr.usersprovider.vo.UserVO;
import com.instanr.common.beans.ActiveUser;
import com.instanr.common.beans.PageVO;

import java.util.List;

/***
 * {@link SysUser}表Service
 * @author instanr
 * @since 2021-10-22
 */
public interface UserService {

    /***
     * 查询用户列表
     * @return List<>{@link SysUser}
     */
    List<UserVO> getUserList(SysUser user);


    PageVO<UserVO> getUserList(SysUser user, Integer pageSize, Integer pageNum);

    /***
     * 通过用户名查询用户
     * @param username 用户名
     * @return {@link SysUser}
     */
    SysUser findUserByName(String username);

    /***
     * 用户基本信息
     * @return {@link ActiveUser}
     */
    UserVO userInfo(String username);

    /***
     * 创建新用户
     * @param user 用户信息
     * @return 成功与否
     */
    Boolean createUser(SysUser user);

    /***
     * 删除用户
     * @param user 用户信息
     * @return 成功与否
     */
    Boolean delete(SysUser user);

    /***
     * 修改用户基本信息
     * @param user 用户基本信息
     * @return 成功与否
     */
    Boolean update(SysUser user);

    /***
     * 修改密码
     * @param password 密码
     * @return 成功与否
     */
    Boolean changePassword(String password, String oldPass, String authorization);

    /***
     * 修改用户状态
     * @param user 用户信息
     * @return 成功与否
     */
    Boolean changeStatus(SysUser user);

    /***
     * 获取当前登录的用户
     * @param authorization token
     * @return {@link UserVO}
     */
    UserVO getActiveUser(String authorization);
}

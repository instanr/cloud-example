package com.instanr.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.instanr.common.beans.ActiveUser;
import com.instanr.common.beans.PageVO;
import com.instanr.common.beans.RedisSaveModel;
import com.instanr.common.consts.SysConstants;
import com.instanr.common.eunm.ErrorCodeEnum;
import com.instanr.common.exception.RunningException;
import com.instanr.common.utils.JWTUtil;
import com.instanr.common.utils.MD5Util;
import com.instanr.user.feign.CacheFeign;
import com.instanr.user.mapper.UserMapper;
import com.instanr.user.service.MenuService;
import com.instanr.user.service.RoleService;
import com.instanr.user.service.UserService;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.db.SysUser;
import com.instanr.usersprovider.vo.UserVO;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;
    private RoleService roleService;
    private MenuService menuService;

    private CacheFeign cacheFeign;

    @Override
    public List<UserVO> getUserList(SysUser user) {
        return null;
    }

    @Override
    public PageVO<UserVO> getUserList(SysUser user, Integer pageSize, Integer pageNum) {
        return null;
    }

    @Override
    public SysUser findUserByName(String username) {
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.lambda().eq(SysUser::getUsername, username);
        return userMapper.selectOne(userQueryWrapper);
    }

    @Override
    public UserVO userInfo(String username) {
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.lambda().eq(SysUser::getUsername, username);
        SysUser user = userMapper.selectOne(userQueryWrapper);
        List<SysRole> roles = roleService.findRolesByUserId(user.getId());
        List<SysMenu> menus = menuService.getMenuListByRoles(roles);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        userVO.setRoles(roles);
        userVO.setMenus(menus);
        return userVO;
    }

    @Override
    public Boolean createUser(SysUser user) {
        if (user == null || !StringUtils.hasText(user.getUsername())) {
            throw new RunningException(ErrorCodeEnum.BODY_NOT_MATCH, "缺少必要参数");
        }
        SysUser userByName = findUserByName(user.getUsername());
        if (userByName != null) {
            throw new RunningException(ErrorCodeEnum.FORBIDDEN, "用户名已存在");
        }
        // 生成盐
        String salt = UUID.randomUUID().toString().replaceAll("-", "");
        String truePassword = MD5Util.md5Encryption(user.getPassword(), salt);
        SysUser createdUser = new SysUser();
        BeanUtils.copyProperties(user, createdUser);
        createdUser.setCreateTime(LocalDateTime.now());
        createdUser.setSalt(salt);
        createdUser.setPassword(truePassword);
        int insert = userMapper.insert(createdUser);
        if (insert != 1) {
            throw new RunningException(ErrorCodeEnum.INTERNAL_SERVER_ERROR, "服务器异常,请稍后再试");
        }
        return true;
    }

    @Override
    public Boolean delete(SysUser user) {
        return null;
    }

    @Override
    public Boolean update(SysUser user) {
        return null;
    }

    @Override
    public Boolean changePassword(String password, String oldPass, String authorization) {
        UserVO activeUser = this.getActiveUser(authorization);
        // 先验证旧密码是否正确
        String trueOldPass = MD5Util.md5Encryption(oldPass, activeUser.getSalt());
        if (!trueOldPass.equals(activeUser.getPassword())){
            throw new RunningException("原密码不正确");
        }
        // 生成盐
        String salt = UUID.randomUUID().toString().replaceAll("-", "");
        // 新密码
        String trueNewPassword = MD5Util.md5Encryption(password, salt);
        SysUser user = new SysUser(){{
            setPassword(trueNewPassword);
            setSalt(salt);
            setId(activeUser.getId());
        }};
        return userMapper.updateById(user) > 0;
    }

    @Override
    public Boolean changeStatus(SysUser user) {
        return null;
    }

    @Override
    public UserVO getActiveUser(String authorization) {
        String username = JWTUtil.getUsername(authorization)[0];
        RedisSaveModel data = cacheFeign.getData(new RedisSaveModel(SysConstants.USER_LOGIN_REDIS_KEY_HEAD + username));
        Map<String, Object> cacheData = (Map<String, Object>) data.getData();
        Object info = cacheData.get("userInfo");
        String s = JSON.toJSONString(info);
        JSONObject jsonObject = JSON.parseObject(s);
        SysUser user = jsonObject.toJavaObject(SysUser.class);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        userVO.setRoles(jsonObject.getJSONArray("roles").toJavaList(SysRole.class));
        userVO.setMenus(jsonObject.getJSONArray("menus").toJavaList(SysMenu.class));
        return jsonObject.toJavaObject(UserVO.class);
    }
}

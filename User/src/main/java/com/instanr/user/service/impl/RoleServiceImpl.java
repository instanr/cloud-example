package com.instanr.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.instanr.common.beans.PageVO;
import com.instanr.user.mapper.RoleMapper;
import com.instanr.user.service.RoleService;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.db.SysRole;
import com.instanr.usersprovider.vo.RoleVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private RoleMapper roleMapper;

    @Override
    public List<SysRole> findRolesByUserId(Long userId) {
        return roleMapper.getRolesByUserId(userId);
    }

    @Override
    public Boolean add(SysRole role) {
        return null;
    }

    @Override
    public Boolean update(SysRole role) {
        return null;
    }

    @Override
    public Boolean delete(SysRole role) {
        return null;
    }

    @Override
    public Boolean setRoleMenus(Long id, List<Long> menus) {
        return null;
    }

    @Override
    public Boolean giveRoleToUsers(Long id, List<Long> users) {
        return null;
    }

    @Override
    public List<RoleVO> getRoleLis(SysRole role) {
        return null;
    }

    @Override
    public PageVO<RoleVO> getRoleLis(SysRole role, Integer pageSize, Integer pageNum) {
        return null;
    }
}

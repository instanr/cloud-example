package com.instanr.user.controller;

import com.instanr.common.beans.RedisSaveModel;
import com.instanr.common.beans.ResponseBean;
import com.instanr.common.utils.JWTUtil;
import com.instanr.user.feign.CacheFeign;
import com.instanr.user.service.UserService;
import com.instanr.usersprovider.vo.UserVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CacheFeign cacheFeign;

    @GetMapping("/getUser")
    public ResponseBean getUser(@RequestParam("username") String username) {
        return ResponseBean.success(userService.findUserByName(username));
    }

    @GetMapping("/getUserInfo")
    public ResponseBean getUserInfo(@RequestParam("username") String username) {
        return ResponseBean.success(userService.userInfo(username));
    }

    @GetMapping("/getActiveUser")
    public ResponseBean getActiveUser(@RequestHeader("Authorization") String authorization) {
        return ResponseBean.success(userService.getActiveUser(authorization));
    }

    @PostMapping("/changePassword")
    public ResponseBean changePassword(@RequestBody UserVO userVO,
                                       @RequestHeader("Authorization") String authorization) {
        if (userService.changePassword(userVO.getPassword(), userVO.getOldPassword(), authorization)) {
            return ResponseBean.success();
        }else {
            return ResponseBean.error("网络错误，请稍后再试");
        }
    }
}

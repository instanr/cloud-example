package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/***
 * 角色-菜单表Mapper
 * @author instanr
 * @since 2021-10-27
 * @see SysRoleMenu
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<SysRoleMenu> {
}

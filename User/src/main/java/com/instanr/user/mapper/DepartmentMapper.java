package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysDepartment;
import org.apache.ibatis.annotations.Mapper;

/***
 * 机构表Mapper
 * @author instanr
 * @since 2021-10-28
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<SysDepartment> {
}

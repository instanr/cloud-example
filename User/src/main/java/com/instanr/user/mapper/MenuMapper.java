package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysMenu;
import com.instanr.usersprovider.vo.RoleMenuVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/***
 * 菜单表Mapper
 * @author instanr
 * @since 2021-10-27
 * @see SysMenu
 */
@Mapper
public interface MenuMapper extends BaseMapper<SysMenu> {

    /***
     * 通过角色id列表查询菜单
     * @param roleIds 角色id
     * @return {@link List}<{@link SysMenu}>
     */
    List<SysMenu> getMenuListByIds(List<Long> roleIds);

    List<RoleMenuVO> getMenuRoleList();
}

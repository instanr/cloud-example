package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/***
 * 角色表Mapper
 * @author instanr
 * @since 2021-10-27
 * @see SysRole
 */
@Mapper
public interface RoleMapper extends BaseMapper<SysRole> {

    List<SysRole> getRolesByUserId(@Param("userId") Long userId);
}

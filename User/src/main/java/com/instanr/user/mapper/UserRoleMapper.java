package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/***
 * 用户-角色表Mapper
 * @author instanr
 * @since 2021-10-27
 * @see SysUserRole
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<SysUserRole> {

    int insertUsersByRole(@Param("roleId") Long roleId, @Param("users") List<Long> users);
}

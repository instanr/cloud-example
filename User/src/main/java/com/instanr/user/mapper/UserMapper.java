package com.instanr.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.instanr.usersprovider.db.SysUser;
import org.apache.ibatis.annotations.Mapper;

/***
 * 用户表Mapper
 * <br>
 * 关联实体{@link User}
 * @author instanr
 * @since 2021-10-22
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
}

package com.instanr.user.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/***
 * Feign 配置类
 * @author instanr
 * @since 2022-03-24
 */
@Slf4j
@Configuration
public class FeignConfig {

    /***
     * 重写 requestInterceptor
     * 目的在于使Feign 的请求也带有原始请求的请求头
     * @return {@link RequestInterceptor}
     */
    @Bean("requestInterceptor")
    public RequestInterceptor requestInterceptor(){
        return template -> {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (attributes == null) {
                return;
            }
            HttpServletRequest request = ((ServletRequestAttributes) attributes).getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();
                    Enumeration<String> values = request.getHeaders(name);
                    while (values.hasMoreElements()) {
                        String value = values.nextElement();
                        // 这里需要注意：
                        // 排除掉 content-length 字段是因为在部分请求中原请求的长度与实际 Feign 请求长度不一致，
                        // 这样会导致报错：feign.RetryableException: too many bytes written executing ....
                        if (name.equals("content-length")){
                            continue;
                        }
                        template.header(name, value);
                    }
                }
            }
        };
    }
}

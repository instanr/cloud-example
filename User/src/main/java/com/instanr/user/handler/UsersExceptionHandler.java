package com.instanr.user.handler;

import com.instanr.common.handler.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UsersExceptionHandler extends GlobalExceptionHandler {
}

package com.instanr.user;

import com.instanr.user.service.UserService;
import com.instanr.usersprovider.db.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserApplicationTests {

    @Autowired
    UserService userService;

    /***
     * 创建用户
     */
    @Test
    void createUser() {
        SysUser user = new SysUser() {{
            setUsername("admin1");
            setPassword("123456");
            setNickname("管理员");
        }};
        Boolean isCreate = userService.createUser(user);
        System.out.println("创建：" + isCreate);
    }

}
